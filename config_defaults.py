# At top of file for easy modification of defaults by users
# Changes to this will need to be re-appli
default_inventory_config_file = '~/.bookinventory.conf'
# The default format of yaml will only work with pyyaml installed
# otherwise it will silently fall back to json
default_inventory_config_data_format = 'yaml'
default_inventory_config_data_file = '~/.book_inventory_data'
