#! /usr/bin/env python3
import cmd
import copy
import maddautils as utils
from maddautils.validate import valid_isbn


class BookInterface(cmd.Cmd):
    """
        An interface for adding or editing books
    """
    def __init__(self, inventory, adding_books=True, book_to_edit_id='-1'):
        """
            Initialise the book interface.
            Requires an instance of the book inventory.
            Defaults to adding books, but if a book ID for editing is
            specified then that book will be edited and adding mode disabled.
        """
        self.inventory = inventory
        self.set_defaults(adding_books, book_to_edit_id)

        if self.book_id != '-1':
            # If we have been told to edit a book we're not adding books
            adding_books = False

        cmd.Cmd.__init__(self)

    def set_defaults(self, adding_books=True, book_id='-1'):
        self.adding_books = adding_books
        self.verifying = False
        self.book = {}
        self.book_id = book_id
        self.title = ''
        self.authors = []
        self.location = ''

    def preloop(self):
        if self.adding_books:
            self.prompt = 'Add a book: '
            self.cmdqueue.append('set_uid')
        else:
            # We should load the book we've been told to edit
            self.load_book(self.book_id)
            self.book_to_edit_id = self.book_id
            self.prompt = 'Edit %s: ' % self.book['title']
            print('')
            self.onecmd('show')
            print('')
            print("Type 'help' for help.")
            print("Type 'save' to save changes.")
            print("Type 'abort' to stop editing without saving changes.")
            print('')

    def load_book(self, book_id):
        """
            Load a book for editing.
            The ID of the book must be specified.
        """
        if book_id in self.inventory.inventory.keys():
            self.book_id = book_id
            self.book = self.inventory.inventory[book_id]
            self.title = copy.deepcopy(self.book['title'])
            self.authors = copy.deepcopy(self.book['authors'])
            self.location = copy.deepcopy(self.book['location'])
            return True
        else:
            return False

    def do_show(self, line):
        """
            Show the settings for the current book.
        """
        if self.book_id != '-1':
            if valid_isbn(self.book_id):
                print('ISBN: %s' % self.book_id)
            else:
                print('UID: %s' % self.book_id)
        else:
            print('No UID set.')

        if self.title:
            print('Title: %s' % self.title)
        else:
            print('No title set.')

        if len(self.authors) > 0:
            print('Authors:')
            for author in self.authors:
                print('  %s' % author)
        else:
            print('No authors set.')

        if self.location:
            print('Location: %s' % self.location)
        else:
            print('No location set.')

    def help_show(self):
        """
            Describe how the show command works.
        """
        print('Show the set values for the current book.')

    def do_set_title(self, line):
        """
            Set the title of this book.
        """
        self.title = utils.request_if_not_set(
            user_input=line,
            prompt="Enter the book's title: ",
            valid_types=('string',)
        )

        if self.adding_books and not self.verifying:
            self.cmdqueue.append('add_author')

    def help_set_title(self):
        """
            Description for the set title command.
        """
        print('Set the title.')
        print('If the title is not supplied, it will be requested.')

    def do_set_uid(self, line):
        """
            Set the UID- intended for setting the ISBN.
            One will be automatically generated if none are specified.
        """
        if self.adding_books and not self.verifying:
            print('If you enter incorrect information, ', end='')
            print('or want to stop, just follow through the prompts.')
            print('At the end you will be asked whether you want to save.')
            print('If you say no, you will be asked to check the details.')
            print("You can then edit them, or 'abort'.")

        uid = utils.request_if_not_set(
            user_input=line,
            prompt='Enter the ISBN or leave blank to generate a UID: ',
            valid_types=('empty_string', 'integer_string')
        )

        current_uids_full = [
            int(uid) for uid in self.inventory.inventory
        ]
        current_uids_no_isbn = [
            int(uid) for uid in current_uids_full
            if uid < 1000000000
        ]
        current_uids_string = [
            uid for uid in self.inventory.inventory
        ]

        if uid == '':
            if len(current_uids_no_isbn) == 0:
                # This is the first non-ISBN UID
                uid = '0'
            else:
                # Get all current uids that aren't ISBNs
                for new_uid in range(0, max(current_uids_no_isbn) + 1):
                    if new_uid not in current_uids_no_isbn:
                        uid = str(new_uid)
                        break

        # Check whether this UID is a duplicate
        if uid in current_uids_string:
            print('This appears to be a duplicate.')
            print('Please verify by searching for the ID: %s' % uid)
            print('Not adding with this UID.')

            if self.adding_books and not self.verifying:
                self.cmdqueue.append('set_uid')
        else:
            self.book_id = uid
            if self.adding_books and not self.verifying:
                self.cmdqueue.append('set_title')

    def help_set_uid(self):
        """
            Describe the set_uid command.
        """
        print('Sets the UID for this book- e.g. the ISBN.')
        print('Will auto-generate a UID if none is specified.')
        print('Checks for duplicates and will not set a duplicate UID.')

    def do_add_author(self, line):
        """
            Add an author to the list of authors for this book.
        """
        author = utils.request_if_not_set(
            user_input=line,
            prompt='Enter name of author to add: ',
            valid_types=('string',)
        )

        similar_authors = self.inventory.search_similar_authors(author)

        if author not in similar_authors:
            # We don't want to prompt if a known author is entered,
            # and spelled correctly- but otherwise, we'll check for typos
            if len(similar_authors) > 0:
                print('Found similar authors. Did you mean:')
                selected_author = utils.choose_item_from_list(
                    items=similar_authors,
                    prompt=
                    'Select an author. Select None to use the entered value:'
                )
                if selected_author >= 0:
                    author = similar_authors[selected_author]

        if author in self.authors:
            print('%s is already listed as an author for this book.' % author)
        else:
            self.authors.append(author)

        if self.adding_books and not self.verifying:
            if utils.prompt_for_confirmation(
                prompt='Add another author?',
                default='n'
            ):
                self.cmdqueue.append('add_author')
            else:
                self.cmdqueue.append('set_location')

    def help_add_author(self):
        """
            Describe the add_author command.
        """
        print('Add an author to this book.')
        print('Will check for similar authors to reduce typos.')
        print('Will not add an author to the book more than once.')

    def do_remove_author(self, line):
        """
            Remove an author from the list of authors on this book.
        """
        author = utils.request_if_not_set(
            user_input=line,
            prompt='Enter name of author to remove: ',
            valid_types=('string',)
        )

        if author not in self.authors:
            print("Could not find %s in this book's authors." % author)
            print('Currently set authors:')
            for current_author in self.authors:
                print('  %s' % current_author)
        else:
            self.authors.remove(author)

    def help_remove_author(self):
        """
            Describe the remove_author command.
        """
        print('Remove an author from the authors currently set on this book.')
        print('Will complain if the specified author is not on this book.')
        print('Will not attempt to correct typos.')

    def do_set_location(self, line):
        """
            Set the location for this book.
        """
        location = utils.request_if_not_set(
            user_input=line,
            prompt='Enter the location for this book: ',
            valid_types=('string',)
        )

        similar_locations = self.inventory.search_similar_locations(location)

        if location not in similar_locations:
            # We don't want to prompt if a known location is entered,
            # and spelled correctly- but otherwise, we'll check for typos
            if len(similar_locations) > 0:
                print('Found similar location names. Did you mean:')
                selected_location = utils.choose_item_from_list(
                    items=similar_locations,
                    prompt=
                    'Select a location, or None to use the entered value:'
                )
                if selected_location >= 0:
                    location = similar_locations[selected_location]

        self.location = location

        if self.adding_books and not self.verifying:
            print('Location set.')

            # Now show the currently set book and prompt for confirmation
            # before saving.
            print()
            self.onecmd('show')

            print()
            print('You can now save, or change the book details.')
            print('If you answer no to saving then you will be asked', end='')
            print(' to amend the details. You can then also choose to abort.')
            if utils.prompt_for_confirmation(prompt='Save this book?'):
                self.cmdqueue.append('save')
            else:
                self.verifying = True
                print('Please correct the details of this book.')
                print("When you are finished, type 'save'.")
                self.onecmd('show')

    def help_set_location(self):
        """
            Describe the set_location command.
        """
        print('Set the location in which this book resides.')
        print('Will check for similar locations to reduce typos.')

    def save_data(self, filename='/var/inventory/books'):
        if self.inventory.save_data('/var/inventory/books'):
            self.inventory.update_authors()
            self.inventory.update_locations()
            self.inventory.update_titles()
            if self.adding_books:
                self.verifying = False
                if utils.prompt_for_confirmation(
                    prompt='Add another book? ',
                    default='y'
                ):
                    self.set_defaults()
                    self.adding_books = True
                    self.cmdqueue.append('set_uid')
                else:
                    return True
            else:
                return True
        else:
            print('Failed to save.')

            if len(self.inventory.collisions) > 0:
                print('There were collisions.')
                print("Abort and type 'collisions' to see them.")
                print('This will tell you how to fix them.')
            else:
                print('Could not lock the file- please try again.')
            print('The changes have NOT been saved.')

    def do_save(self, line):
        """
            Add this book and save.
        """
        duplicate_check = {
            '-1': {
                'title': self.title,
                'location': self.location,
                'authors': self.authors
            }
        }

        duplicates = self.inventory.find_duplicates(duplicate_check)
        confirmed_save = False
        if len(duplicates) == 0:
            # No need to confirm with no duplicates
            confirmed_save = True
        else:
            if len(duplicates) == 1:
                print('Possible duplicate found in existing inventory:')
            else:
                print('Possible duplicates found in existing inventory:')
            for uid, book in duplicates:
                print('%s' % book['title'])
                print('By: %s' % ', '.join(book['authors']))
                print('Location: %s' % book['location'])
                print('')

            confirmed_save = utils.prompt_for_confirmation(
                prompt='Do you still wish to save? ',
                default='n'
            )

        if confirmed_save:
            if self.adding_books:
                self.inventory.add_book(
                    self.title,
                    self.location,
                    self.authors,
                    uid=self.book_id
                )
                if self.save_data():
                    print('Thank you for adding books.')
                    return True
            else:
                # In case we have changed the ID, we will delete the book
                self.inventory.delete_book(self.book_to_edit_id)
                self.save_data()
                self.inventory.add_book(
                    self.title,
                    self.location,
                    self.authors,
                    self.book_id
                )
                if self.save_data():
                    print('Changes saved.')
                    return True
        else:
            print('Book not saved. Make further changes or abort.')

    def help_save(self):
        """
            Describe the save command.
        """
        print("Save this book's details.")

    def do_abort(self, line):
        """
            Stop adding/editing book(s).
        """
        return True

    def help_abort(self):
        """
            Describe the abort command.
        """
        if self.adding_books:
            print('Stop adding books, and do not save this one.')
        else:
            print('Stop editing this book, without saving it.')
